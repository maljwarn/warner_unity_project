﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary{
	public float xmin, xmax, zmin, zmax;
}
public class PlayerControler : MonoBehaviour {
	private Rigidbody rb;
	public float speed;
	public float tilt;
	public Boundary boundary;
	public GameObject shot;
	public Transform shotSpawn;
	public GameObject Mshot;
	public Transform MshotSpawn;
	public float fireRate;
	private float nextFire;
	public float MfireRate;
	private float MnextFire;
	private AudioSource audioSource;
	//public GameObject heart;

	public GUIText speedText;



	void Update (){
		if (Input.GetButton ("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
//			GameObject clone = 
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			audioSource.Play ();
		}
		if (Input.GetButton ("Fire2") && Time.time > MnextFire) {
			MnextFire = Time.time + MfireRate;
			//			GameObject clone = 
			Instantiate(Mshot, MshotSpawn.position, MshotSpawn.rotation);
			audioSource.Play ();
		}
	
	
	
	}



	void Start (){
		rb = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource> ();
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		UpdateSpeed ();

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;

		rb.position = new Vector3 (
			Mathf.Clamp (rb.position.x, boundary.xmin, boundary.xmax),
			0.0f,
			Mathf.Clamp (rb.position.z, boundary.zmin, boundary.zmax));
		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);

		//if (speed = 6) {
		//	if (!GameObject.Find ("GameController").GetComponent<GameController> ().bossBattle);
		//	GameController.bossBattle = true;
		//}
	}



	void OnTriggerEnter (Collider other) 
	{
		if (other.tag == "Heart") {
			if (speed < 10) {
				speed += 2;
			}
		}
		Destroy(other.gameObject);
	}

	void UpdateSpeed (){
		speedText.text = "Speed: " + speed;
	}
}
