﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	public float health;
	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
				gameController = gameControllerObject.GetComponent <GameController>();

		}

		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other) 
	{
		if (other.tag == "Boundary" || other.tag == "Enemy"|| other.tag == "Boss") 
		{
			return;
		}
		if (other.tag == "PlayerBolt") {
			health -= 1;
		}
		if (other.tag == "Missle") {
			health -= 3;
		}

		if (explosion != null)
		{
			Instantiate(explosion, transform.position, transform.rotation);
		}

		if (other.tag == "Player") 
		{
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver ();
		}

		//if (health = 0){ 
		
		gameController.AddScore (scoreValue);

		//}

		if (health <= 0) {
			print ("HEALTH 0" + tag);
			if (tag == "Boss") {
				print ("YEAH BOSS!");
				GameObject.Find ("Player").GetComponent<PlayerControler>().speed = 2;
			}
			Destroy (gameObject);
		}


		Destroy(other.gameObject);

		//Destroy (gameObject);
	}
		
}
