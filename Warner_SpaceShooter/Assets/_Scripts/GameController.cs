﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject[] hazards;
	public Vector3 spawnValue;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText controllerText;

	private bool gameOver;
	private bool restart;
	private int score;

	public GameObject boss;

	public bool bossBattle;


	void Start () {
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		bossBattle = false;
	}

	void Update() 
	{
		//if (GameObject.Find ("Player").GetComponent<PlayerControler> ().bossBattle) {
	
		if (!gameOver) {
			if (GameObject.Find ("Player").GetComponent<PlayerControler> ().speed == 10 && !bossBattle) {
				bossBattle = true;
				print ("Boss");
				SpawnBoss ();

		
			}
		}


		if (restart) 
		{ 
			if (Input.GetButton ("Submit")) 
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
	}
		
	IEnumerator SpawnWaves(){
		yield return new WaitForSeconds (startWait);
		while(true)
		{
			for (int i = 0;i < hazardCount; i++) {
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				restartText.text = "Press 'Enter' for Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (int newScoreValue){
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore (){
		scoreText.text = "Score: " + score;
	}

	public void GameOver ()
	{
		gameOverText.text = "You Died";
		gameOver = true;
	}
	void SpawnBoss()
	{
		Vector3 bossSpawnPosition = new Vector3 (Random.Range (-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
		Instantiate(boss, bossSpawnPosition, transform.rotation);
		//bossBattle = false;

	


	}
}
